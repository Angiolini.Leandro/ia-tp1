export const config = {
    dataEntrenar: '../Data/entrenar.csv',
    dataEvaluar: '../Data/evaluar.csv',
    salidaResultadoEvaluar: '../Salidas/resultadoEvaluado.csv',
    guardarDirectorio: '../Salidas',
    tamMaximoPalabra: 27,
    epocas: 10,
    muestrasPorActualizacion: 50,
    capasOcultas: [50, 25],
    porcentajeUsandoParaValidacionEntrenamiento: 0
}