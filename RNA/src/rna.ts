import * as tf from '@tensorflow/tfjs-node'
import * as data from './cargarDatos'
import { normalizar } from './utils/normalizar'
import { config } from '../config'
import * as path from 'path'

interface Tensores {
    x: tf.Tensor3D
    y: tf.Tensor2D
}

// Tamaño tamMaximoPalabra x 27
export function palabraToOneHot(palabra: string) {
    return Array.from(normalizar(palabra)).concat(Array(config.tamMaximoPalabra - palabra.length).fill(' ')).map(e => Array.from("abcdefghijklmnñopqrstuvwxyz").map(l => l == e ? 1 : 0))
}

export class ModeloIdentificarPalabras {
    modelo: tf.Sequential

    async iniciar() {
        this.modelo = tf.sequential()
    }

    private crearTensores(palabras: data.PalabraValor[]) {
        const lista_x = palabras.map(e => palabraToOneHot(e.palabra))
        const lista_y = palabras.map(e => e.entra ? [0, 1] : [1, 0])

        const x = tf.tensor3d(lista_x, [lista_x.length, lista_x[0].length, lista_x[0][0].length])
        const y = tf.tensor2d(lista_y, [lista_y.length, lista_y[0].length])

        return { x, y }
    }

    async agregarCapas() {
        this.modelo.add(tf.layers.conv1d({
            filters: 128,
            kernelSize: [3],
            inputShape: [config.tamMaximoPalabra, 27],
            activation: 'relu',
            padding: 'same',
            useBias: false
        }))

        this.modelo.add(tf.layers.globalMaxPooling1d())

        for (const neuronas of config.capasOcultas) {
            this.modelo.add(tf.layers.dense({ units: neuronas, activation: 'sigmoid', useBias: false }))
        }

        this.modelo.add(tf.layers.dense({ units: 2, activation: 'sigmoid', useBias: false }))
    }

    async compilar() {
        this.modelo.compile({ optimizer: 'adam', loss: tf.losses.absoluteDifference, metrics: ['accuracy'] })
    }

    async entrenarModelo() {
        const { x, y } = this.crearTensores(await data.cargarEntrenar())
        await this.modelo.fit(x, y, {
            epochs: config.epocas,
            batchSize: config.muestrasPorActualizacion,
            validationSplit: config.porcentajeUsandoParaValidacionEntrenamiento
        })
    }

    async evaluarDatos() {
        const palabras = await data.cargarEvaluar()
        const result = palabras.map(p => ({
            ...p,
            resultado: !!(this.modelo.predict(this.crearTensores([p]).x) as tf.Tensor).argMax(1).arraySync()[0]
        }))
        const guardar = result.reduce((t, a) => {
            return t + `${a.palabra},${a.resultado},${a.entra},${a.resultado == a.entra}\n`
        }, "palabra,resultado obtenido,resultado esperado,comparacion\n")
        
        data.guardar(guardar)
    }

    async resumen() {
        this.modelo.summary()
    }

    async guardarModelo() {
        this.modelo.save('file://' + path.resolve(config.guardarDirectorio))
    }

    async cargarModelo() {
        this.modelo = await tf.loadLayersModel('file://' + path.resolve(config.guardarDirectorio, 'model.json')) as tf.Sequential
    }

    entra(palabra: string): boolean {
        const result = this.modelo.predict(tf.tensor3d([palabraToOneHot(palabra)])) as tf.Tensor<tf.Rank>
        console.log(result.dataSync())
        return !!result.argMax(1).dataSync()[0]
    }
}