import * as readline from 'readline'
import { normalizar } from './normalizar';

let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

function readPromise(): Promise<string> {
    return new Promise((resolve, reject) => {
        rl.question('Palabra a probar: ', palabra => {
            resolve(palabra)
        });
    })
}

export async function ObtenerPalabra() {
	const expresionAdmitidas = /^[a-z,ñ,Ã,À,Á,Ä,Â,È,É,Ë,Ê,Ì,Í,Ï,Î,Ò,Ó,Ö,Ô,Ù,Ú,Ü,Û,ã,à,á,ä,â,è,é,ë,ê,ì,í,ï,î,ò,ó,ö,ô,ù,ú,ü,û]+$/
	const palabra = (await readPromise()).toLowerCase()
	if (!palabra.match(expresionAdmitidas)) {
		throw Error('La palabra no debe contener espacios y solo caracteres entre a - z')
	}
	return normalizar(palabra)
}