export function normalizar (str: string) {
    const from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÇç"
    const to = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuucc"

    return str.split('').map(e => {
        if(from.indexOf(e) !== -1) {
            return to[from.indexOf(e)]
        }
        return e
    } ).join('')
}