import csv from 'csv-parser'
import * as fs from 'fs'

export function readCsv(filePath) {
    return new Promise((resolve, reject) => {
        const data = []
        const stream = fs.createReadStream(filePath).pipe(csv())

        stream.on('data', (row) => data.push(row))
        stream.on('end', () => resolve(data))
        stream.on('error', (err) => reject(err))
    })
}

export function saveCsv(filePath, data) {
    fs.writeFileSync(filePath, data)
}