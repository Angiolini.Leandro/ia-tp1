import { ModeloIdentificarPalabras } from './src/rna'

(async function() {
	const identificador = new ModeloIdentificarPalabras()
	await identificador.cargarModelo()
	await identificador.compilar()
	await identificador.resumen()
	await identificador.evaluarDatos()
})()
