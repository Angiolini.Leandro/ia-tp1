import { readCsv, saveCsv } from './utils/csv'
import { config } from '../config'
import * as path from 'path'

export interface PalabraValor {
    palabra: string
    entra: boolean
}

function cargarDatos(file): Promise<PalabraValor[]> {
    return readCsv(path.resolve(file)).then((lista: any[]) => lista.map(fila => ({palabra: fila.palabra, entra: fila.entra == 'true'})))
}

export function cargarEntrenar() {
    return cargarDatos(config.dataEntrenar)
}

export function cargarEvaluar() {
    return cargarDatos(config.dataEvaluar)
}

export function guardar(data: string) {
    saveCsv(path.resolve(config.salidaResultadoEvaluar), data)
}