import { ModeloIdentificarPalabras, palabraToOneHot } from './src/rna'

(async function() {
	const identificador = new ModeloIdentificarPalabras()
	await identificador.iniciar()
	await identificador.agregarCapas()
	await identificador.compilar()
	await identificador.resumen()
	await identificador.entrenarModelo()
	await identificador.guardarModelo()
})()
