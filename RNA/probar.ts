import { ModeloIdentificarPalabras } from './src/rna'
import { ObtenerPalabra } from './src/utils/leerConsola'

(async function () {
	const identificador = new ModeloIdentificarPalabras()
	await identificador.cargarModelo()
	await identificador.compilar()
	await identificador.resumen()

	while (true) {
		try {
			const palabra = await ObtenerPalabra()
			const resultado = identificador.entra(palabra)
			console.log(`\x1b[${resultado?32:31}m%s\x1b[0m`, `La palabra '${palabra}' ${resultado?' ':' no '}entra en la caja`)
		} catch (e) { console.log(e.message) }
	}
})()
